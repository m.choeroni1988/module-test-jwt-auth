<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table style="border: solid 1px grey; margin-bottom: 20px; padding: 20px;">
        <tr>
            <td>ID</td>
            <td>:</td>
            <td>{{ $user['id'] }}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>:</td>
            <td>{{ $user['name'] }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{ $user['email'] }}</td>
        </tr>
        <tr>
            <td>Role</td>
            <td>:</td>
            <td>{{ $user['role'] }}</td>
        </tr>
    </table>
    <table style="border: solid 1px grey; margin-bottom: 20px; padding: 20px;">
        <tr>
            <td colspan="3">Organization</td>
        </tr>
        <tr>
            <td>ID</td>
            <td>:</td>
            <td>{{ $organization['id'] }}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>:</td>
            <td>{{ $organization['name'] }}</td>
        </tr>
    </table>

    <table style="border: solid 1px grey; margin-bottom: 20px; padding: 20px;">
        <tr>
            <td colspan="3">Group</td>
        </tr>
        <tr>
            <td>ID</td>
            <td>:</td>
            <td>{{ $group['id'] }}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>:</td>
            <td>{{ $group['name'] }}</td>
        </tr>
    </table>
</body>
</html>
