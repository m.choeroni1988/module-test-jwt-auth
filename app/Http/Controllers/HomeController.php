<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        // Check Auth
        // $response = Http::withHeaders(
        //         [
        //             "Authorization" => "bearer " . $request->token,
        //             "Accept" => "application/json",
        //         ]
        //     )
        //     ->post('http://core.test/api/v1/jwt/me', []);

        // $bodyResponse = $response->getBody();
        // $result = $bodyResponse->getContents();
        // $result = json_decode($result);
        // dd($response->json());

        list($header, $payload, $signature) = explode('.', $request->token);
        $jsonToken = base64_decode($payload);
        $arrayToken = json_decode($jsonToken, true);


        return view('show', $arrayToken);

    }
}
